# açık tcp portlarını listeler
sudo netstat -tulpn | grep LISTEN

# node üzerinde aktif çalışan proces'leri gösterir
ps aux

# node üzerinde free kalan memory miktarını gösterir.
free -m

# process id'si bilinen processi öldürmece
kill -9 <process-id>

# 5000 tcp portunda çalışan process'in id'sinin bulunması
lsof -t -i:5000

# devops script for test, build, deploy via scp and run with ssh
cd ../
mvn install
scp ./target/customerapi-0.0.1-SNAPSHOT.jar root@207.154.216.113:/
echo 'customerapi-0.0.1-SNAPSHOT.jar send to bugra-kube'
ssh root@207.154.216.113 'java -Dserver.port=5050 -jar customerapi-0.0.1-SNAPSHOT.jar'