# platform bağımlı ci-cd automation tool

# derle kardeşim for maven root
cd /Users/tugberk.celik/Desktop/bugra-teaching/repos/customer-product-rest-contoller-api

# testleri çalıştırıyor, build(.class), jar dosyası oluşturmaca
mvn clean install

# oluşan jar, release ilgili sunucuya gönderir blocking
sshpass -p $1 scp /Users/tugberk.celik/Desktop/bugra-teaching/repos/customer-product-rest-contoller-api/target/customer-product-rest-contoller-api.jar root@207.154.216.113:/releases

# uzak sunucuya bağlanır ve oradaki 5000 portunda çalışan processi kill eder.
sshpass -p $1  ssh root@207.154.216.113 'kill -9 $(lsof -t -i:5000)'

# uzak sunucuya bağlanır ve orada java -jar komutunu çalıştırır
sshpass -p $1  ssh root@207.154.216.113 'java -Dserver.port=5000 -jar /releases/customer-product-rest-contoller-api.jar'