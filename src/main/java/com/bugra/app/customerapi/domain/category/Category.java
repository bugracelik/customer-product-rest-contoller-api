package com.bugra.app.customerapi.domain.category;

import com.bugra.app.customerapi.domain.product.Product;

import java.util.ArrayList;
import java.util.List;

// model
public class Category { // one 1 category birden fazla ürün ( liste tarzı collection ) olabilir
    private Integer CategoryID;
    private String CategoryName;
    private String Description;

    // one to many liste var one tarafında
    private List<Product> productList = new ArrayList<>(); // key nokta

    public Category() {
    }

    public Category(Integer categoryID, String categoryName, String description) {
        CategoryID = categoryID;
        CategoryName = categoryName;
        Description = description;
    }

    public Integer getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(Integer categoryID) {
        CategoryID = categoryID;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    @Override
    public String toString() {
        return "Category{" +
                "CategoryID=" + CategoryID +
                ", CategoryName='" + CategoryName + '\'' +
                ", Description='" + Description + '\'' +
                '}';
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }
}
