package com.bugra.app.customerapi.domain.product;

import com.bugra.app.customerapi.infra.request.product.CreateProductRequest;
import com.bugra.app.customerapi.infra.request.product.UpdateProductRequest;

import java.sql.SQLException;
import java.util.List;

public abstract interface IProductRepository {
    public List<Product> getAllProducts() throws SQLException;

    public Product getProductById(Integer id) throws SQLException;

    public List<Product> getProductByName(String name) throws SQLException;

    public void deleteProductById(Integer id) throws SQLException;

    public Product findMinPrice() throws SQLException;

    public double getMinPrice() throws SQLException;

    public List<Product> getProductPriceByBetween(double number1, double number2) throws SQLException;

    public List<Product> findProductsByPrice(double price) throws SQLException;

    void createProductWithCreateProductRequest(CreateProductRequest createProductRequest) throws SQLException;

    void updateProductWithUpdateProductRequest(UpdateProductRequest updateProductRequest, int id) throws SQLException;
}
