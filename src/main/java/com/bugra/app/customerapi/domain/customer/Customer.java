package com.bugra.app.customerapi.domain.customer;

public class Customer {

    private Integer customer_id;
    private String customer_name;
    private String customer_surname;
    private String csutomer_adress;
    private String customer_city;
    private String customer_email;
    private String customer_password;

    public Customer() {
    }

    public Customer(Integer customer_id, String customer_name, String customer_surname, String csutomer_adress, String customer_city, String customer_email, String customer_password) {
        this.customer_id = customer_id;
        this.customer_name = customer_name;
        this.customer_surname = customer_surname;
        this.csutomer_adress = csutomer_adress;
        this.customer_city = customer_city;
        this.customer_email = customer_email;
        this.customer_password = customer_password;
    }

    public Integer getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(Integer customer_id) {
        this.customer_id = customer_id;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_surname() {
        return customer_surname;
    }

    public void setCustomer_surname(String customer_surname) {
        this.customer_surname = customer_surname;
    }

    public String getCsutomer_adress() {
        return csutomer_adress;
    }

    public void setCsutomer_adress(String csutomer_adress) {
        this.csutomer_adress = csutomer_adress;
    }

    public String getCustomer_city() {
        return customer_city;
    }

    public void setCustomer_city(String customer_city) {
        this.customer_city = customer_city;
    }

    public String getCustomer_email() {
        return customer_email;
    }

    public void setCustomer_email(String customer_email) {
        this.customer_email = customer_email;
    }

    public String getCustomer_password() {
        return customer_password;
    }

    public void setCustomer_password(String customer_password) {
        this.customer_password = customer_password;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customer_id=" + customer_id +
                ", customer_name='" + customer_name + '\'' +
                ", customer_surname='" + customer_surname + '\'' +
                ", csutomer_adress='" + csutomer_adress + '\'' +
                ", customer_city='" + customer_city + '\'' +
                ", customer_email='" + customer_email + '\'' +
                ", customer_password='" + customer_password + '\'' +
                '}';
    }
}
