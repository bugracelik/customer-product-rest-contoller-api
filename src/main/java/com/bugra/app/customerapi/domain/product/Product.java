package com.bugra.app.customerapi.domain.product;

import com.bugra.app.customerapi.domain.category.Category;

public class Product { // many tarafı

    private int id;
    private String name;
    private int supplierId;
    private int categoryId; // 16
    private String unit;
    private double price;

    // bir tane o kategori
    private Category category; // key

    public Product() {
    }

    public Product(int id, String name, int supplierId, int categoryId, String unit, double price) {
        this.id = id;
        this.name = name;
        this.supplierId = supplierId;
        this.categoryId = categoryId;
        this.unit = unit;
        this.price = price;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getSupplierId() {
        return supplierId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public String getUnit() {
        return unit;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", supplierId=" + supplierId +
                ", categoryId=" + categoryId +
                ", unit='" + unit + '\'' +
                ", price=" + price +
                '}';
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
