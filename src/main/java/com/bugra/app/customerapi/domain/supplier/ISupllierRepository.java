package com.bugra.app.customerapi.domain.supplier;

import com.bugra.app.customerapi.infra.request.supplier.CreateSupplierRequest;
import com.bugra.app.customerapi.infra.request.supplier.UpdateSupplierRequest;

import java.sql.SQLException;
import java.util.ArrayList;

public abstract interface ISupllierRepository {

    public ArrayList<Supplier> getAllSuppliersFromDb() throws SQLException ;

    public Supplier getSuppliersByID(Integer id) throws SQLException;

    public Supplier getSuppliersByContactName(String name) throws SQLException;

    public void createSupplier(String supplierName, String contactName, String adress, String city, String postalCode, String country) throws SQLException;

    public void updateSupplier(Integer supplierId, String supplierName, String contactName, String adress, String city, String postalCode, String country) throws SQLException;

    public void deleteSupplierById(int supplierId) throws SQLException;

    void createSupplier(CreateSupplierRequest createSupplierRequest) throws SQLException;

    void updateSupplier(UpdateSupplierRequest updateSupplierRequest, int id) throws SQLException;
}
