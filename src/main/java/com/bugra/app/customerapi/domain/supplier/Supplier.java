package com.bugra.app.customerapi.domain.supplier;

public class Supplier {

    private Integer SupplierId;
    private String SupplierName;
    private String ContactName;
    private String Address;
    private String City;
    private String PostalCode;
    private String Country;

    public Supplier() {
    }

    public Supplier(Integer supplierId, String supplierName, String contactName, String address, String city, String postalCode, String country) {
        SupplierId = supplierId;
        SupplierName = supplierName;
        ContactName = contactName;
        Address = address;
        City = city;
        PostalCode = postalCode;
        Country = country;
    }

    public Integer getSupplierId() {
        return SupplierId;
    }

    public void setSupplierId(Integer supplierId) {
        SupplierId = supplierId;
    }

    public String getSupplierName() {
        return SupplierName;
    }

    public void setSupplierName(String supplierName) {
        SupplierName = supplierName;
    }

    public String getContactName() {
        return ContactName;
    }

    public void setContactName(String contactName) {
        ContactName = contactName;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getPostalCode() {
        return PostalCode;
    }

    public void setPostalCode(String postalCode) {
        PostalCode = postalCode;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    @Override
    public String toString() {
        return "Supplier{" +
                "SupplierId=" + SupplierId +
                ", SupplierName='" + SupplierName + '\'' +
                ", ContactName='" + ContactName + '\'' +
                ", Address='" + Address + '\'' +
                ", City='" + City + '\'' +
                ", PostalCode='" + PostalCode + '\'' +
                ", Country='" + Country + '\'' +
                '}';
    }
}
