package com.bugra.app.customerapi.application.services.category;

import com.bugra.app.customerapi.domain.category.Category;
import com.bugra.app.customerapi.domain.category.ICategoryRepository;
import com.bugra.app.customerapi.infra.request.category.CreateCategoryRequest;
import com.bugra.app.customerapi.infra.request.category.DeleteCategoryRequest;
import com.bugra.app.customerapi.infra.request.category.UpdateCategoryRequest;
import com.bugra.app.customerapi.infra.repository.category.CategoryRepositoryImpl;
import java.sql.SQLException;
import java.util.ArrayList;

public class CategoryServiceImpl implements ICategoryService {

    private final static ICategoryRepository categoryRepository = new CategoryRepositoryImpl();

    @Override
    public ArrayList<Category> getAllCategoryFromDb() throws SQLException {
        return categoryRepository.getAllCategoryFromDb();
    }

    @Override
    public Category getCategoryByID(Integer id) throws SQLException {
        return categoryRepository.getCategoryByID(id);
    }

    @Override
    public ArrayList<Category> getCategoryByCategoryName(String name) throws SQLException {
        return categoryRepository.getCategoryByCategoryName(name);

    }

    @Override
    public Category getCategoryByIdWithProductWithInnerJoinQuery(Integer id) throws SQLException {
        return categoryRepository.getCategoryByIdWithProductWithInnerJoinQuery(id);
    }

    @Override
    public void createCategoryWithCreateRequest(CreateCategoryRequest createCategoryRequest) throws SQLException {
        categoryRepository.createCategoryWithCreateRequest(createCategoryRequest);
    }

    @Override
    public void updateCategoryWithUpdateRequest(UpdateCategoryRequest updateCategoryRequest) throws SQLException {
        categoryRepository.updateCategoryWithUpdateRequest(updateCategoryRequest);
    }

    @Override
    public void deleteCategoryWithDeleteRequest(DeleteCategoryRequest deleteCategoryRequest) throws SQLException {
        categoryRepository.deleteCategoryWithDeleteRequest(deleteCategoryRequest);
    }

}
