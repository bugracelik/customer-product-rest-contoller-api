package com.bugra.app.customerapi.application.services.customer;

import com.bugra.app.customerapi.domain.customer.Customer;
import com.bugra.app.customerapi.infra.request.customer.CreateCustomerRequest;
import com.bugra.app.customerapi.infra.request.customer.DeleteCustomerRequest;
import com.bugra.app.customerapi.infra.request.customer.UpdateCustomerRequest;

import java.sql.SQLException;
import java.util.ArrayList;

public interface ICustomerService {
    ArrayList<Customer> getAllCustomerFromDb() throws SQLException;

    ArrayList<Customer> getCustomerByName(String name) throws SQLException;

    Customer getCustomerByID(Integer id) throws SQLException;

    ArrayList<Customer> getCustomerByNameAndSurname(String name, String surname) throws SQLException;

    Integer count() throws SQLException;

    void createCustomerWithCreateRequest(CreateCustomerRequest createCustomerRequest) throws SQLException;

    void updateCustomerWithPutRequest(UpdateCustomerRequest updateCustomerRequest, Integer id) throws SQLException;

    void deleteCustomerWithDeleteRequest(DeleteCustomerRequest deleteCustomerRequest, Integer id) throws SQLException;
}
