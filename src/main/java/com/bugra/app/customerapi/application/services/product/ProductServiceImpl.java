package com.bugra.app.customerapi.application.services.product;

import com.bugra.app.customerapi.domain.product.IProductRepository;
import com.bugra.app.customerapi.domain.product.Product;
import com.bugra.app.customerapi.infra.repository.product.ProductRepositoryImpl;
import com.bugra.app.customerapi.infra.request.product.CreateProductRequest;
import com.bugra.app.customerapi.infra.request.product.UpdateProductRequest;

import java.sql.SQLException;
import java.util.List;

public class ProductServiceImpl implements IProductService {

    private IProductRepository productRepository = new ProductRepositoryImpl();

    public List<Product> getAllProducts() throws SQLException {
        List<Product> allProducts = productRepository.getAllProducts();
        return allProducts;
    }

    public Product getProductById(Integer id) throws SQLException {
        return productRepository.getProductById(id);
    }

    @Override
    public List<Product> getProductByName(String name) throws SQLException {
        return productRepository.getProductByName(name);
    }

    @Override
    public void deleteProductById(Integer id) throws SQLException {
        productRepository.deleteProductById(id);
    }

    @Override
    public Double getMinPrice() throws SQLException {
        return productRepository.getMinPrice();
    }

    @Override
    public List<Product> getProductPriceByBetween(double number1, double number2) throws SQLException {
        return productRepository.getProductPriceByBetween(number1, number2);
    }

    @Override
    public List<Product> findProductsByPrice(double price) throws SQLException {
        return productRepository.findProductsByPrice(price);
    }

    @Override
    public void createProductWithCreateProductRequest(CreateProductRequest createProductRequest) throws SQLException {
        productRepository.createProductWithCreateProductRequest(createProductRequest);
    }

    @Override
    public void updateProductWithUpdateProductRequest(UpdateProductRequest updateProductRequest, int id) throws SQLException {
        productRepository.updateProductWithUpdateProductRequest(updateProductRequest, id);
    }

}
