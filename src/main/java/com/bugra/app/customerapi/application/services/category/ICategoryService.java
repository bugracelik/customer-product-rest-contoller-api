package com.bugra.app.customerapi.application.services.category;

import com.bugra.app.customerapi.domain.category.Category;
import com.bugra.app.customerapi.infra.request.category.CreateCategoryRequest;
import com.bugra.app.customerapi.infra.request.category.DeleteCategoryRequest;
import com.bugra.app.customerapi.infra.request.category.UpdateCategoryRequest;

import java.sql.SQLException;
import java.util.ArrayList;

public interface ICategoryService {
    ArrayList<Category> getAllCategoryFromDb() throws SQLException;

    Category getCategoryByID(Integer id) throws SQLException;

    ArrayList<Category> getCategoryByCategoryName(String name) throws SQLException;

    Category getCategoryByIdWithProductWithInnerJoinQuery(Integer id) throws SQLException;

    void createCategoryWithCreateRequest(CreateCategoryRequest createCategoryRequest) throws SQLException;

    void updateCategoryWithUpdateRequest(UpdateCategoryRequest updateCategoryRequest) throws SQLException;

    void deleteCategoryWithDeleteRequest(DeleteCategoryRequest deleteCategoryRequest) throws SQLException;

}
