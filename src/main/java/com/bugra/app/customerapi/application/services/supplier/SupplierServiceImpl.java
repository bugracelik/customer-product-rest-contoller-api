package com.bugra.app.customerapi.application.services.supplier;

import com.bugra.app.customerapi.domain.supplier.ISupllierRepository;
import com.bugra.app.customerapi.domain.supplier.Supplier;
import com.bugra.app.customerapi.infra.repository.supplier.SupplierRepositoryImpl;
import com.bugra.app.customerapi.infra.request.supplier.CreateSupplierRequest;
import com.bugra.app.customerapi.infra.request.supplier.UpdateSupplierRequest;

import java.sql.SQLException;
import java.util.ArrayList;

public class SupplierServiceImpl implements ISupplierService {

    ISupllierRepository supllierRepository = new SupplierRepositoryImpl();

    public ArrayList<Supplier> getAllSuppliersFromDb() throws SQLException {
        return supllierRepository.getAllSuppliersFromDb();
    }

    @Override
    public Supplier getSuppliersByID(Integer id) throws SQLException {
        return supllierRepository.getSuppliersByID(id);
    }

    @Override
    public Supplier getSuppliersByContactName(String name) throws SQLException {
        return supllierRepository.getSuppliersByContactName(name);
    }

    @Override
    public void createSupplier(String SupplierName, String ContactName, String Adress, String City, String PostalCode, String Country) throws SQLException {
        supllierRepository.createSupplier(SupplierName, ContactName, Adress, City, PostalCode, Country);
    }

    @Override
    public void updateSupplier(Integer SupplierId, String SupplierName, String ContactName, String Adress, String City, String PostalCode, String Country) throws SQLException {
        supllierRepository.updateSupplier(SupplierId, SupplierName, ContactName, Adress, City, PostalCode, Country);
    }

    @Override
    public void deleteSupplierById(int SupplierId) throws SQLException {
        supllierRepository.deleteSupplierById(SupplierId);
    }

    @Override
    public void createSupplier(CreateSupplierRequest createSupplierRequest) throws SQLException {
        supllierRepository.createSupplier(createSupplierRequest);
    }

    @Override
    public void updateSupplier(UpdateSupplierRequest updateSupplierRequest, int id) throws SQLException {
        supllierRepository.updateSupplier(updateSupplierRequest, id);
    }
}
