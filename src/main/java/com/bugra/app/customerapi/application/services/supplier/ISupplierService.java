package com.bugra.app.customerapi.application.services.supplier;

import com.bugra.app.customerapi.domain.supplier.Supplier;
import com.bugra.app.customerapi.infra.request.supplier.CreateSupplierRequest;
import com.bugra.app.customerapi.infra.request.supplier.UpdateSupplierRequest;

import java.sql.SQLException;
import java.util.ArrayList;

public interface ISupplierService {
    ArrayList<Supplier> getAllSuppliersFromDb() throws SQLException;
    Supplier getSuppliersByID(Integer id) throws SQLException;
    Supplier getSuppliersByContactName(String ContactName) throws SQLException;
    void createSupplier(String SupplierName, String ContactName, String Adress, String City, String PostalCode, String Country) throws SQLException;
    void updateSupplier(Integer SupplierId, String SupplierName, String ContactName, String Adress, String City, String PostalCode, String Country) throws SQLException;
    void deleteSupplierById(int SupplierId) throws SQLException;
    void createSupplier(CreateSupplierRequest createSupplierRequest) throws SQLException;
    void updateSupplier(UpdateSupplierRequest updateSupplierRequest, int id) throws SQLException;
}
