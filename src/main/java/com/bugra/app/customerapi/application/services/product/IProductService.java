package com.bugra.app.customerapi.application.services.product;

import com.bugra.app.customerapi.domain.product.Product;
import com.bugra.app.customerapi.infra.request.product.CreateProductRequest;
import com.bugra.app.customerapi.infra.request.product.UpdateProductRequest;

import java.sql.SQLException;
import java.util.List;

public interface IProductService {
    List<Product> getAllProducts() throws SQLException;

    Product getProductById(Integer id) throws SQLException;

    List<Product> getProductByName(String name) throws SQLException;

    void deleteProductById(Integer id) throws SQLException;

    Double getMinPrice() throws SQLException;

    List<Product> getProductPriceByBetween(double number1, double number2) throws SQLException;

    List<Product> findProductsByPrice(double price) throws SQLException;

    void createProductWithCreateProductRequest(CreateProductRequest createProductRequest) throws SQLException;

    void updateProductWithUpdateProductRequest(UpdateProductRequest updateProductRequest, int id) throws SQLException;
}
