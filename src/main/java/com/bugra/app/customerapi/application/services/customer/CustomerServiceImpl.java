package com.bugra.app.customerapi.application.services.customer;


import com.bugra.app.customerapi.domain.customer.Customer;
import com.bugra.app.customerapi.domain.customer.ICustomerRepository;
import com.bugra.app.customerapi.infra.request.customer.CreateCustomerRequest;
import com.bugra.app.customerapi.infra.request.customer.DeleteCustomerRequest;
import com.bugra.app.customerapi.infra.request.customer.UpdateCustomerRequest;
import com.bugra.app.customerapi.infra.repository.customer.CustomerRepositoryImpl;

import java.sql.SQLException;
import java.util.ArrayList;

public class CustomerServiceImpl implements ICustomerService {
    private ICustomerRepository customerRepository = new CustomerRepositoryImpl();

    @Override
    public ArrayList<Customer> getAllCustomerFromDb() throws SQLException {
        return customerRepository.getAllCustomerFromDb();
    }

    @Override
    public ArrayList<Customer> getCustomerByName(String name) throws SQLException {
        return customerRepository.getCustomerByName(name);
    }

    @Override
    public Customer getCustomerByID(Integer id) throws SQLException {
        return customerRepository.getCustomerById(id);
    }

    @Override
    public ArrayList<Customer> getCustomerByNameAndSurname(String name, String surname) throws SQLException {
        return customerRepository.getCustomerByNameAndSurname(name, surname);
    }

    @Override
    public Integer count() throws SQLException {
        Integer result = customerRepository.count();
        return result;
    }

    @Override
    public void createCustomerWithCreateRequest(CreateCustomerRequest createCustomerRequest) throws SQLException {
        customerRepository.createCustomerWithCreateRequest(createCustomerRequest);
    }

    @Override
    public void updateCustomerWithPutRequest(UpdateCustomerRequest updateCustomerRequest, Integer id) throws SQLException {
        customerRepository.updateCustomerWithPutRequest(updateCustomerRequest, id);
    }

    @Override
    public void deleteCustomerWithDeleteRequest(DeleteCustomerRequest deleteCustomerRequest, Integer id) throws SQLException {
        customerRepository.deleteCustomerWithDeleteRequest(deleteCustomerRequest, id);
    }
}
