package com.bugra.app.customerapi.infra.constants;

public class HtmlConstants {
    public static final String CATEGORIES_HTML = "<!DOCTYPE html>\n" +
            "<html>\n" +
            "<head>\n" +
            "<title></title>\n" +
            "</head>\n" +
            "<body>\n" +
            "<table border=\"1\" style=\"border-collapse:collapse\">\n" +
            "<tr><th>CategoryID</th><th>CategoryName</th><th>Description</th></tr>\n" +
            "<tr><td>1</td><td>fermente</td><td>fermente icki</td></tr>\n" +
            "<tr><td>2</td><td>Condiments-foo</td><td>Sweet and savory sauces, relishes, spreads, and seasonings</td></tr>\n" +
            "<tr><td>3</td><td>Confections</td><td>Desserts, candies, and sweet breads</td></tr>\n" +
            "<tr><td>4</td><td>Dairy Product</td><td>Cheeses</td></tr>\n" +
            "<tr><td>5</td><td>Grains/Cereals</td><td>Breads, crackers, pasta, and cereal</td></tr>\n" +
            "<tr><td>6</td><td>Meat/Poultry</td><td>Prepared meats</td></tr>\n" +
            "<tr><td>7</td><td>eglence</td><td>top, bilardo</td></tr>\n" +
            "<tr><td>8</td><td>Seafood</td><td>Seaweed and fish</td></tr>\n" +
            "<tr><td>9</td><td>beyaz eşya</td><td>çamaşır makinesi, bulaşık makinesi, buz dolabı</td></tr>\n" +
            "<tr><td>11</td><td>bilgisayar</td><td>laptop,dizüstü,monitor</td></tr>\n" +
            "<tr><td>16</td><td>ahsap</td><td>mobilya, mobilya, tornavida</td></tr>\n" +
            "<tr><td>19</td><td>asda</td><td>sdasd</td></tr>\n" +
            "<tr><td>23</td><td>altın</td><td>mücevher</td></tr>\n" +
            "<tr><td>24</td><td>gümüş</td><td>mücevher</td></tr>\n" +
            "</table>\n" +
            "</body>\n" +
            "</html>\n";;
}
