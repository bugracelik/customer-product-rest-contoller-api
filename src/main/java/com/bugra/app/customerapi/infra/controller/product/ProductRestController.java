package com.bugra.app.customerapi.infra.controller.product;

import com.bugra.app.customerapi.domain.product.Product;
import com.bugra.app.customerapi.application.services.product.IProductService;
import com.bugra.app.customerapi.application.services.product.ProductServiceImpl;
import com.bugra.app.customerapi.infra.mapper.ProductMapper;
import com.bugra.app.customerapi.infra.model.ProductResponse;
import com.bugra.app.customerapi.infra.request.product.CreateProductRequest;
import com.bugra.app.customerapi.infra.request.product.UpdateProductRequest;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
public class ProductRestController {

    private final static IProductService productService = new ProductServiceImpl();

    @GetMapping("products")
    public List<ProductResponse> getAllProducts() throws SQLException {
        List<Product> allProducts = productService.getAllProducts();
        return ProductMapper.productMapForList(allProducts);
    }

    @GetMapping("getProductById/{id}")
    public ProductResponse getProductById(@PathVariable Integer id) throws SQLException {
        Product product = productService.getProductById(id);
        return ProductMapper.productMapForOneProduct(product);
    }

    @GetMapping("getProductByName/{name}")
    public List<ProductResponse> getProductByName(@PathVariable String name) throws SQLException {
        List<Product> productByName = productService.getProductByName(name);
        return ProductMapper.productMapForList(productByName);
    }

    @GetMapping("getMinPrice")
    public Double getMinPrice() throws SQLException {
        return productService.getMinPrice();
    }

    @GetMapping("getProductPriceByBetween/{number1}/{number2}")
    public List<ProductResponse> getProductPriceByBetween(@PathVariable double number1, @PathVariable double number2) throws SQLException {
        List<Product> productPriceByBetween = productService.getProductPriceByBetween(number1, number2);
        return ProductMapper.productMapForList(productPriceByBetween);
    }

    @GetMapping("findProductsByPrice/{price}")
    public List<ProductResponse> findProductsByPrice(@PathVariable double price) throws SQLException {
        List<Product> productsByPrice = productService.findProductsByPrice(price);
        return ProductMapper.productMapForList(productsByPrice);
    }

    @PostMapping("createProduct")
    public void createProductWithCreateProductRequest(@RequestBody CreateProductRequest createProductRequest) throws SQLException {
        productService.createProductWithCreateProductRequest(createProductRequest);
    }

    @PutMapping("updateProduct/{id}")
    public void updateProductWithUpdateProductRequest(@RequestBody UpdateProductRequest updateProductRequest, @PathVariable int id) throws SQLException {
        productService.updateProductWithUpdateProductRequest(updateProductRequest, id);
    }

    @DeleteMapping("deleteProduct/{id}")
    public void deleteProduct(@PathVariable int id) throws SQLException {
        productService.deleteProductById(id);
    }

}
