package com.bugra.app.customerapi.infra.controller.supplier;

import com.bugra.app.customerapi.domain.supplier.Supplier;
import com.bugra.app.customerapi.application.services.supplier.ISupplierService;
import com.bugra.app.customerapi.application.services.supplier.SupplierServiceImpl;
import com.bugra.app.customerapi.infra.request.supplier.CreateSupplierRequest;
import com.bugra.app.customerapi.infra.request.supplier.UpdateSupplierRequest;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.ArrayList;

@RestController
public class SupplierRestController {

    ISupplierService supplierService = new SupplierServiceImpl();

    @GetMapping("getAllSuppliersFromDb")
    public ArrayList<Supplier> getAllSuppliersFromDb() throws SQLException {
        return supplierService.getAllSuppliersFromDb();
    }

    @GetMapping("getSuppliersByID/{id}")
    public Supplier getSuppliersByID(@PathVariable Integer id) throws SQLException {
        return supplierService.getSuppliersByID(id);
    }

    @GetMapping("getSuppliersByContactName/{name}")
    public Supplier getSuppliersByContactName(@PathVariable String name) throws SQLException {
        return supplierService.getSuppliersByContactName(name);
    }

    @PostMapping("createSupplier")
    public void createSupplier(@RequestBody CreateSupplierRequest createSupplierRequest) throws SQLException {
        supplierService.createSupplier(createSupplierRequest);
    }

    @PutMapping("updateSupplier/{id}")
    public void updateSupplier(@RequestBody UpdateSupplierRequest updateSupplierRequest, @PathVariable int id) throws SQLException {
        supplierService.updateSupplier(updateSupplierRequest, id);
    }

    @DeleteMapping("deleteSupplier/{id}")
    public void deleteSupplierById(@PathVariable int id) throws SQLException {
        supplierService.deleteSupplierById(id);
    }

}

