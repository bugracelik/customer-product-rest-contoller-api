package com.bugra.app.customerapi.infra.request.category;

public class CreateCategoryRequest {

    private String categoryName;
    private String description;

    public CreateCategoryRequest() {
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
