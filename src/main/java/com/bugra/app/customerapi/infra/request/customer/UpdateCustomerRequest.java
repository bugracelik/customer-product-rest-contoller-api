package com.bugra.app.customerapi.infra.request.customer;

public class UpdateCustomerRequest {
    private String name;
    private String surname;
    private String city;
    private String address;
    private String email;
    private String password;

    public UpdateCustomerRequest() {
    }

    public UpdateCustomerRequest(String name, String surname, String city, String address, String email, String password) {
        this.name = name;
        this.surname = surname;
        this.city = city;
        this.address = address;
        this.email = email;
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
