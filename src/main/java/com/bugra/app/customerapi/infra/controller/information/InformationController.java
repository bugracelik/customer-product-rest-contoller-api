package com.bugra.app.customerapi.infra.controller.information;


import com.bugra.app.customerapi.infra.model.ServerIpDto;
import org.springframework.context.annotation.Primary;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InformationController {

    @GetMapping("serverip")
    public String serverIpInformation() {
        return new ServerIpDto().getServerIp();
    }

    @GetMapping("serverip-test")
    public String serverIpInformationTest() {
        return new ServerIpDto().getServerIp();
    }

    @GetMapping("kafka-ok")
    public String kafka() {
        return "kafka-ok";
    }

    // feature to deploy
    @GetMapping("couchbase-ok")
    public String cb() {
        return "cb-ok";
    }

    // feature to deploy
    @GetMapping("new")
    public String new_() {
        return "new";
    }

    // feature to deploy
    @GetMapping("deploy")
    public String deploy() {
        return "deploy";
    }

    // feature to deploy
    @GetMapping("deploy-api")
    public String deploy_api() {
        return "deploy_Api";
    }

    // feature to deploy
    @GetMapping("deploy-api-with-git-repo-name")
    public String deploy_api_repo_name() {
        return "deploy-api-with-git-repo-name";
    }

    // feature to deploy
    @GetMapping("sıcak-sıcak")
    public String sıcak() {
        return "sıcak";
    }

    // feature to deploy
    @GetMapping("cold")
    public String cold() {
        return "cold";
    }

    // feature to deploy
    @PostMapping("cold")
    public String cold_() {
        return "cold";
    }
}



