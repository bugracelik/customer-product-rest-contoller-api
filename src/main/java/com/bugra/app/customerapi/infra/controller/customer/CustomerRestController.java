package com.bugra.app.customerapi.infra.controller.customer;

import com.bugra.app.customerapi.domain.customer.Customer;
import com.bugra.app.customerapi.infra.request.customer.CreateCustomerRequest;
import com.bugra.app.customerapi.infra.request.customer.DeleteCustomerRequest;
import com.bugra.app.customerapi.infra.request.customer.UpdateCustomerRequest;
import com.bugra.app.customerapi.infra.model.CustomerCountInfo;
import com.bugra.app.customerapi.application.services.customer.CustomerServiceImpl;
import com.bugra.app.customerapi.application.services.customer.ICustomerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.ArrayList;

@RestController
public class CustomerRestController {

    private final static ICustomerService customerService = new CustomerServiceImpl();

    @GetMapping("getAllCustomersFromDb")
    public ArrayList<Customer> getAllCustomersFromDb() throws SQLException {
        return customerService.getAllCustomerFromDb();
    }

    @GetMapping("customer-by-name/{name}")
    public ArrayList<Customer> getCustomerByName(@PathVariable String name) throws SQLException {
        return customerService.getCustomerByName(name);
    }

    @GetMapping("customer-by-id-seri/{id}")
    public ResponseEntity getCustomerByIDSeri(@PathVariable Integer id) throws SQLException, JsonProcessingException {
        Customer customerByID = customerService.getCustomerByID(id);

        // seri etmece
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonCustomer = objectMapper.writeValueAsString(customerByID);

        new ResponseEntity("json", HttpStatus.OK);
        ResponseEntity<String> responseWithHeaderAndBody = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(jsonCustomer);
        ResponseEntity responseEntity = new ResponseEntity("json", HttpStatus.OK);

        return new ResponseEntity("json", HttpStatus.OK);
    }

    @GetMapping("customer-by-id/{id}")
    public Customer getCustomerByID(@PathVariable Integer id) throws SQLException {
        Customer customerByID = customerService.getCustomerByID(id);
        return customerByID; // 200 OK, body json string seri etmece
    }


    @GetMapping("customer-by-name-and-surname/{name}/{surname}")
    public ArrayList<Customer> getCustomerByNameAndSurname(@PathVariable String name, @PathVariable String surname) throws SQLException {
        return customerService.getCustomerByNameAndSurname(name, surname);
    }

    @PostMapping("createCustomer")
    public void createCustomerWithCreateRequest(@RequestBody CreateCustomerRequest createCustomerRequest) throws SQLException {
        customerService.createCustomerWithCreateRequest(createCustomerRequest);
    }


    @PutMapping("updateCustomer/{id}")
    public void updateCustomer(@RequestBody UpdateCustomerRequest updateCustomerRequest, @PathVariable Integer id) throws SQLException {
        customerService.updateCustomerWithPutRequest(updateCustomerRequest, id);
    }

    @DeleteMapping("deleteCustomer/{id}")
    public void deleteCustomerWithDeleteRequest(@RequestBody DeleteCustomerRequest deleteCustomerRequest, @PathVariable Integer id) throws SQLException {
        customerService.deleteCustomerWithDeleteRequest(deleteCustomerRequest, id);
    }

    @GetMapping("customer/count")
    public CustomerCountInfo count() throws SQLException {
        Integer count = customerService.count();

        CustomerCountInfo customerCountInfo = new CustomerCountInfo(count); // dto

        return customerCountInfo;
    }


}
