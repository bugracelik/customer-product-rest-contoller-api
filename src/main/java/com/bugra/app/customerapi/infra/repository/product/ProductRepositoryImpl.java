package com.bugra.app.customerapi.infra.repository.product;

import com.bugra.app.customerapi.infra.mysql.ConnectionManager;
import com.bugra.app.customerapi.domain.product.IProductRepository;
import com.bugra.app.customerapi.domain.product.Product;
import com.bugra.app.customerapi.infra.request.product.CreateProductRequest;
import com.bugra.app.customerapi.infra.request.product.UpdateProductRequest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class ProductRepositoryImpl implements IProductRepository {

    public List<Product> getAllProducts() throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("select * from trendyoldb.products");
        ResultSet resultSet = preparedStatement.executeQuery();
        return ResultSetMethod.getAllProductWithResultSet(resultSet);
    }

    public Product getProductById(Integer id) throws SQLException {

        String sql = String.format("select * from trendyoldb.products where id = %d", id);

        Connection connection = ConnectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();

        return ResultSetMethod.getAllProductWithResultSetUniq(resultSet);
    }

    public List<Product> getProductByName(String name) throws SQLException {

        //aynı üründen birden fazla olabilir satıcısı farklı olup aynı ürün olabilir

        String sql = String.format("select * from trendyoldb.products where name = '%s'", name);
        ResultSet resultSet = ConnectionManager.getConnection().prepareStatement(sql).executeQuery();

        return ResultSetMethod.getAllProductWithResultSet(resultSet);
    }

    public void deleteProductById(Integer id) throws SQLException {
        String sql = String.format("delete from trendyoldb.products where id = %d", id);
        ConnectionManager.getConnection().prepareStatement(sql).execute();
    }

    public Product findMinPrice() throws SQLException {
        String sql = "select min(price) from products";
        ResultSet resultSet = ConnectionManager.getConnection().prepareStatement(sql).executeQuery();
        resultSet.next();
        double minprice = resultSet.getDouble("price");
        String format = String.format("select * from trendyoldb.products where price = %f", minprice);
        ResultSet resultSet1 = ConnectionManager.getConnection().prepareStatement(format).executeQuery();
        return ResultSetMethod.getAllProductWithResultSetUniq(resultSet1);
    }

    public double getMinPrice() throws SQLException {
        String sql = "select min(price) as price from trendyoldb.products";
        ResultSet resultSet = ConnectionManager.getConnection().prepareStatement(sql).executeQuery();
        resultSet.next();
        return resultSet.getDouble("price");
    }

    public List<Product> getProductPriceByBetween(double number1, double number2) throws SQLException {
        String sql = String.format("select * from trendyoldb.products  where Price between %f AND %f", number1, number2);
        ResultSet resultSet = ConnectionManager.getConnection().prepareStatement(sql).executeQuery();
        return ResultSetMethod.getAllProductWithResultSet(resultSet);

    }

    public List<Product> findProductsByPrice(double price) throws SQLException {
        String sql = String.format("select * from trendyoldb.products where price = %f", price);
        ResultSet resultSet = ConnectionManager.getConnection().prepareStatement(sql).executeQuery();
        return ResultSetMethod.getAllProductWithResultSet(resultSet);
    }

    @Override
    public void createProductWithCreateProductRequest(CreateProductRequest createProductRequest) throws SQLException {
        /*
        isletim sistemi String.format fonksiyonunu cagırırken double degeri 12,0 olarak alıyor,
        mysql 12.0 istiyor başka veri girişinde exception fırlatıyor,
        ben de String.valueOf metoduyla 12.0 ' ı alıyorum ve String ifademi formatlıyorum.
         */
        String pricef = String.valueOf(createProductRequest.getPrice());
        String sqlWord = String.format("insert into trendyoldb.products (name, supplier_id, category_id, unit, price) values ('%s', %d, %d, '%s'",createProductRequest.getName(),createProductRequest.getSupplier_id(),createProductRequest.getCategory_id(), createProductRequest.getUnit()) + "," + pricef + ")";
        ConnectionManager.getConnection().prepareStatement(sqlWord).execute();
    }

    @Override
    public void updateProductWithUpdateProductRequest(UpdateProductRequest updateProductRequest, int id) throws SQLException {
        /*
        pricef 'in String olduguna dikkat et parse ederken %s koyuyoruz.!!
        isletim sistemi String.format fonksiyonunu cagırırken double degeri 12,0 olarak alıyor,
        mysql 12.0 istiyor başka veri girişinde exception fırlatıyor,
        ben de String.valueOf metoduyla 12.0 ' ı alıyorum ve String ifademi formatlıyorum.
         */
        String pricef = String.valueOf(updateProductRequest.getPrice());
        String sqlWord = String.format("update trendyoldb.products set name = '%s', supplier_id = %d, category_id = %d, unit = '%s', price = %s where id = %d",
                updateProductRequest.getName(),
                updateProductRequest.getSupplier_id(),
                updateProductRequest.getCategory_id(),
                updateProductRequest.getUnit(),
                pricef, id);
        ConnectionManager.getConnection().prepareStatement(sqlWord).execute();
    }
}



