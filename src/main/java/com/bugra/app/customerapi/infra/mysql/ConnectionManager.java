package com.bugra.app.customerapi.infra.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {

    private static  Connection connection;

    // 1 kere java vm
    //deneme
    static {
        try {
            String digitalOceanMySqlServerHost = "mysql-digital-ocean-cluster-do-user-8063408-0.b.db.ondigitalocean.com";
            String port = "25060";
            String schemaName = "/trendyoldb";
            String url = "jdbc:mysql://" + digitalOceanMySqlServerHost + ":" + port + schemaName;
            connection = DriverManager.getConnection(url, "doadmin", "tcuj0v9mconi1cjv");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            // System.exit(1);
        }
    }

    public static Connection getConnection() {
        return connection;
    }

    public static void setConnection(Connection connection){
        ConnectionManager.connection = connection;
    }
}
