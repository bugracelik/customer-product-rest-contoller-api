package com.bugra.app.customerapi.infra.repository.customer;


import com.bugra.app.customerapi.infra.request.customer.CreateCustomerRequest;
import com.bugra.app.customerapi.infra.request.customer.DeleteCustomerRequest;
import com.bugra.app.customerapi.infra.request.customer.UpdateCustomerRequest;
import com.bugra.app.customerapi.infra.mysql.ConnectionManager;
import com.bugra.app.customerapi.domain.customer.ICustomerRepository;
import com.bugra.app.customerapi.domain.customer.Customer;

import java.sql.*;
import java.util.ArrayList;

public class CustomerRepositoryImpl implements ICustomerRepository {

    @Override
    public ArrayList<Customer> getAllCustomerFromDb() throws SQLException {

        int customer_id;
        String customer_name;
        String customer_surname;
        String customer_adress;
        String customer_city;
        String customer_email;
        String customer_password;

        String sqlWord = "select * from trendyoldb.customers";
        ArrayList<Customer> customers = new ArrayList<>();

        Connection connection = ConnectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(sqlWord);
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            customer_id = resultSet.getInt("CustomerID");
            customer_name = resultSet.getString("CustomerName");
            customer_surname = resultSet.getString("CustomerSurname");
            customer_adress = resultSet.getString("CustomerAdress");
            customer_city = resultSet.getString("CustomerCity");
            customer_email = resultSet.getString("CustomerEmail");
            customer_password = resultSet.getString("CustomerPassword");
            Customer customer = new Customer(customer_id, customer_name, customer_surname, customer_adress, customer_city, customer_email, customer_password);
            customers.add(customer);
        }

        return customers;


    }

    @Override // interface işareti anatasyon
    public ArrayList<Customer> getCustomerByName(String name) throws SQLException {

        int customer_id;
        String customer_name;
        String customer_surname;
        String customer_adress;
        String customer_city;
        String customer_email;
        String customer_password;

        String sql = String.format("select * from trendyoldb.customers where CustomerName = '%s'", name);
        ArrayList<Customer> customers = new ArrayList<>();

        Connection connection = ConnectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            customer_id = resultSet.getInt("CustomerID");
            customer_name = resultSet.getString("CustomerName");
            customer_surname = resultSet.getString("CustomerSurname");
            customer_adress = resultSet.getString("CustomerAdress");
            customer_city = resultSet.getString("CustomerCity");
            customer_email = resultSet.getString("CustomerEmail");
            customer_password = resultSet.getString("CustomerPassword");
            Customer customer = new Customer(customer_id, customer_name, customer_surname, customer_adress, customer_city, customer_email, customer_password);
            customers.add(customer);
        }

        // return customers;

        return customers;
    }

    @Override
    public Customer getCustomerById(Integer id) throws SQLException {

        int customer_id;
        String customer_name;
        String customer_surname;
        String customer_adress;
        String customer_city;
        String customer_email;
        String customer_password;

        String sql = String.format("select * from trendyoldb.customers where CustomerID = '%d'", id);

        Connection connection = ConnectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();

        //id primar-key oldugundan resultseti bir kere next yapsam yeter.
        resultSet.next();

        customer_id = resultSet.getInt("CustomerID");
        customer_name = resultSet.getString("CustomerName");
        customer_surname = resultSet.getString("CustomerSurname");
        customer_adress = resultSet.getString("CustomerAdress");
        customer_city = resultSet.getString("CustomerCity");
        customer_email = resultSet.getString("CustomerEmail");
        customer_password = resultSet.getString("CustomerPassword");
        return new Customer(customer_id, customer_name, customer_surname, customer_adress, customer_city, customer_email, customer_password);
    }

    @Override
    public ArrayList<Customer> getCustomerByNameAndSurname(String name, String surname) throws SQLException {

        int customer_id;
        String customer_name;
        String customer_surname;
        String customer_adress;
        String customer_city;
        String customer_email;
        String customer_password;

        String sql = String.format("select * from trendyoldb.customers where CustomerName = '%s' and CustomerSurname = '%s'", name, surname);

        //benim db'im de birden fazla aynı isim ve soyada sahip insanlar olabilir bu yüzden arraylist kullanıyorum.

        ArrayList<Customer> customers = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            customer_id = resultSet.getInt("CustomerID");
            customer_name = resultSet.getString("CustomerName");
            customer_surname = resultSet.getString("CustomerSurname");
            customer_adress = resultSet.getString("CustomerAdress");
            customer_city = resultSet.getString("CustomerCity");
            customer_email = resultSet.getString("CustomerEmail");
            customer_password = resultSet.getString("CustomerPassword");
            Customer customer = new Customer(customer_id, customer_name, customer_surname, customer_adress, customer_city, customer_email, customer_password);
            customers.add(customer);
        }

        return customers;

    }


    @Override
    public Integer count() throws SQLException {

        String countSql = "SELECT count(*) as customer_count FROM customers";
        Connection connection = ConnectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(countSql);
        ResultSet resultSet = preparedStatement.executeQuery();

        //...
        resultSet.next(); // headeri geç 1 satır

        return resultSet.getInt("customer_count");
    }

    @Override
    public void createCustomerWithCreateRequest(CreateCustomerRequest createCustomerRequest) throws SQLException {
        String sqlWord = String.format("insert into trendyoldb.customers(CustomerName, CustomerSurname, CustomerAdress, CustomerCity, CustomerEmail, CustomerPassword) values ('%s', '%s', '%s', '%s', '%s', '%s')",
                createCustomerRequest.getName(),
                createCustomerRequest.getSurname(),
                createCustomerRequest.getAddress(),
                createCustomerRequest.getCity(),
                createCustomerRequest.getEmail(),
                createCustomerRequest.getPassword());
        ConnectionManager.getConnection().prepareStatement(sqlWord).execute();
    }

    @Override
    public void updateCustomerWithPutRequest(UpdateCustomerRequest updateCustomerRequest, Integer id) throws SQLException {
        String sqlWord = String.format("update trendyoldb.customers set CustomerSurname = '%s', CustomerCity ='%s', CustomerName ='%s', CustomerAdress = '%s', CustomerEmail='%s', CustomerPassword = '%s' where CustomerID = %d; ",
                updateCustomerRequest.getSurname(),
                updateCustomerRequest.getCity(),
                updateCustomerRequest.getName(),
                updateCustomerRequest.getAddress(),
                updateCustomerRequest.getEmail(),
                updateCustomerRequest.getPassword(),
                id);
        ConnectionManager.getConnection().prepareStatement(sqlWord).execute();
    }

    @Override
    public void deleteCustomerWithDeleteRequest(DeleteCustomerRequest deleteCustomerRequest, Integer id) throws SQLException {
        String sqlWord = String.format("delete from trendyoldb.customers where CustomerID= %d and  CustomerName = '%s' and CustomerSurname = '%s' and CustomerEmail = '%s';",
                id,
                deleteCustomerRequest.getName(),
                deleteCustomerRequest.getSurname(),
                deleteCustomerRequest.getEmail());
        ConnectionManager.getConnection().prepareStatement(sqlWord).execute();
    }
}