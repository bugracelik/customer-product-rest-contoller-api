package com.bugra.app.customerapi.infra.controller.category;

import com.bugra.app.customerapi.application.services.category.CategoryServiceImpl;
import com.bugra.app.customerapi.application.services.category.ICategoryService;
import com.bugra.app.customerapi.domain.category.Category;
import com.bugra.app.customerapi.domain.product.Product;
import com.bugra.app.customerapi.infra.constants.CategoriesFromCache;
import com.bugra.app.customerapi.infra.constants.HtmlConstants;
import com.bugra.app.customerapi.infra.model.CategoryResponse;
import com.bugra.app.customerapi.infra.model.ProductResponse;
import com.bugra.app.customerapi.infra.request.category.CreateCategoryRequest;
import com.bugra.app.customerapi.infra.request.category.DeleteCategoryRequest;
import com.bugra.app.customerapi.infra.request.category.UpdateCategoryRequest;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryRestController {

    private final static ICategoryService categoryService = new CategoryServiceImpl();

    @GetMapping("getCategory") // 5 sn
    public ArrayList<Category> getCategory() throws SQLException {
        return categoryService.getAllCategoryFromDb();
    }

    @GetMapping("getCategoryByID/{id}")
    public CategoryResponse getCategoryByID(@PathVariable Integer id) throws SQLException {
        Category category = categoryService.getCategoryByID(id); // db'den gelen tüm productlar
        //code buraya geldiginde category nesnesinin içerigi hazırlanmış oldu ve içi dolu oldu.

        // response hazirla
        CategoryResponse response = new CategoryResponse();

        response.setName(category.getCategoryName());
        response.setDescription(category.getDescription());

        List<ProductResponse> productResponseList = new ArrayList<>();

        for (Product p : category.getProductList()) {
            ProductResponse productResponse = new ProductResponse();

            productResponse.setId(p.getId());
            productResponse.setCategoryId(p.getCategoryId());
            productResponse.setCategoryName(p.getCategory().getCategoryName());
            productResponse.setDescription(p.getCategory().getDescription());

            productResponseList.add(productResponse);
        }

        response.setProducts(productResponseList);

        return response;
    }

    @GetMapping("getCategoryByID/{id}/innerjoin")
    public CategoryResponse getCategoryByIDInnerJoin(@PathVariable Integer id) throws SQLException {
        Category category = categoryService.getCategoryByIdWithProductWithInnerJoinQuery(id); // db'den gelen

        // response hazirla
        CategoryResponse response = new CategoryResponse();

        response.setName(category.getCategoryName());
        response.setDescription(category.getDescription());
        response.setCategoryId(category.getCategoryID());

        List<ProductResponse> productResponseList = new ArrayList<>();

        for (Product p : category.getProductList()) {
            ProductResponse productResponse = new ProductResponse();

            productResponse.setId(p.getId());
            productResponse.setCategoryId(p.getCategoryId());
            // productResponse.setCategoryName(p.getCategory().getCategoryName());
            // productResponse.setDescription(p.getCategory().getDescription());

            productResponseList.add(productResponse);
        }

        response.setProducts(productResponseList);

        return response;
    }


    @GetMapping("getCategoryByCategoryName/{name}")
    public ArrayList<Category> getCategoryByCategoryName(@PathVariable String name) throws SQLException {
        return categoryService.getCategoryByCategoryName(name);
    }


    @PostMapping("createCategory")
    public void createCategoryWithCreateRequest(@RequestBody CreateCategoryRequest createCategoryRequest) throws SQLException {
        categoryService.createCategoryWithCreateRequest(createCategoryRequest);
    }


    @PutMapping("updateCategory")
    public void updateCategoryWithUpdateRequest(@RequestBody UpdateCategoryRequest updateCategoryRequest) throws SQLException {
        //deseri işlemi yapılır @RequestBody anastasyonunda...
        categoryService.updateCategoryWithUpdateRequest(updateCategoryRequest);
    }


    @DeleteMapping("deleteCategory")
    public void deleteCategoryWithDeleteRequest(@RequestBody DeleteCategoryRequest deleteCategoryRequest) throws SQLException {
        categoryService.deleteCategoryWithDeleteRequest(deleteCategoryRequest);
    }

    // 10 ms
    @GetMapping("cache/all-categories")
    public String readCategoriesFromCache() {
        return CategoriesFromCache.readCategoriesFromCache();
    }

    // 10 ms
    @GetMapping("cache/all-categories-html")
    public String readCategoriesFromCacheHtml() {
        return HtmlConstants.CATEGORIES_HTML; // red-green-refactor
    }


}






