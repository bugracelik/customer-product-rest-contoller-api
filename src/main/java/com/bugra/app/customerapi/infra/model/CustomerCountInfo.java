package com.bugra.app.customerapi.infra.model;

import java.util.Objects;

public class CustomerCountInfo {
    private Integer count;

    public CustomerCountInfo() {
    }

    public CustomerCountInfo(Integer count) {
        this.count = count;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
