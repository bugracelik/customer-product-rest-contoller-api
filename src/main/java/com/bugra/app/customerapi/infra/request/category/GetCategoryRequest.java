package com.bugra.app.customerapi.infra.request.category;

public class GetCategoryRequest {

    private Integer id;

    public GetCategoryRequest() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
