package com.bugra.app.customerapi.infra.constants;

public class CategoriesFromCache {

    public static String readCategoriesFromCache() {

        String categoriesJsonArray = "[\n" +
                "  {\n" +
                "    \"CategoryID\": 1,\n" +
                "    \"CategoryName\": \"fermente\",\n" +
                "    \"Description\": \"fermente icki\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"CategoryID\": 2,\n" +
                "    \"CategoryName\": \"Condiments-foo\",\n" +
                "    \"Description\": \"Sweet and savory sauces, relishes, spreads, and seasonings\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"CategoryID\": 3,\n" +
                "    \"CategoryName\": \"Confections\",\n" +
                "    \"Description\": \"Desserts, candies, and sweet breads\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"CategoryID\": 4,\n" +
                "    \"CategoryName\": \"Dairy Product\\t\",\n" +
                "    \"Description\": \"Cheeses\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"CategoryID\": 5,\n" +
                "    \"CategoryName\": \"Grains/Cereals\",\n" +
                "    \"Description\": \"Breads, crackers, pasta, and cereal\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"CategoryID\": 6,\n" +
                "    \"CategoryName\": \"Meat/Poultry\\t\",\n" +
                "    \"Description\": \"Prepared meats\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"CategoryID\": 7,\n" +
                "    \"CategoryName\": \"eglence\",\n" +
                "    \"Description\": \"top, bilardo\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"CategoryID\": 8,\n" +
                "    \"CategoryName\": \"Seafood\",\n" +
                "    \"Description\": \"Seaweed and fish\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"CategoryID\": 9,\n" +
                "    \"CategoryName\": \"beyaz eşya\",\n" +
                "    \"Description\": \"çamaşır makinesi, bulaşık makinesi, buz dolabı\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"CategoryID\": 11,\n" +
                "    \"CategoryName\": \"bilgisayar\",\n" +
                "    \"Description\": \"laptop,dizüstü,monitor\"\n" +
                "  }\n" +
                "]";

        return categoriesJsonArray;
    }
}