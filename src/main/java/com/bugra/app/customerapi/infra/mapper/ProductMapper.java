package com.bugra.app.customerapi.infra.mapper;

import com.bugra.app.customerapi.domain.product.Product;
import com.bugra.app.customerapi.infra.model.ProductResponse;
import java.util.ArrayList;
import java.util.List;

public class ProductMapper {


    public static List<ProductResponse> productMapForList(List<Product> allProducts) {

        List<ProductResponse> productResponseList = new ArrayList<>();

        int i = 0;

        while (i < allProducts.size()) {
            ProductResponse productResponse = new ProductResponse();
            productResponse.setId(allProducts.get(i).getId());
            productResponse.setDescription(allProducts.get(i).getCategory().getDescription());
            productResponse.setCategoryId(allProducts.get(i).getCategoryId());
            productResponse.setCategoryName(allProducts.get(i).getName());
            productResponseList.add(productResponse);
            i++;
        }
        return productResponseList;
    }


    public static ProductResponse productMapForOneProduct(Product product) {
        ProductResponse productResponse = new ProductResponse();
        productResponse.setCategoryName(product.getName());
        productResponse.setId(product.getId());
        productResponse.setCategoryId(product.getCategoryId());
        return productResponse;
    }
}
