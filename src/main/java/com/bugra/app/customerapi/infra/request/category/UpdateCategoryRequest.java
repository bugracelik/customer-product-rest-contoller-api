package com.bugra.app.customerapi.infra.request.category;

public class UpdateCategoryRequest {

    private Integer id;
    private String categoryName;
    private String description;

    public UpdateCategoryRequest() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
