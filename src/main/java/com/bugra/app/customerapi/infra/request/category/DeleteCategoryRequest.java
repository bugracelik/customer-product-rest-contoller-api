package com.bugra.app.customerapi.infra.request.category;

public class DeleteCategoryRequest {
    private Integer id;

    public DeleteCategoryRequest(Integer id) {
        this.id = id;
    }

    public DeleteCategoryRequest() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
