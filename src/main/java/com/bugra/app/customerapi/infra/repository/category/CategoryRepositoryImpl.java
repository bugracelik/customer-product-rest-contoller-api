package com.bugra.app.customerapi.infra.repository.category;

import com.bugra.app.customerapi.infra.request.category.CreateCategoryRequest;
import com.bugra.app.customerapi.infra.request.category.DeleteCategoryRequest;
import com.bugra.app.customerapi.infra.request.category.UpdateCategoryRequest;
import com.bugra.app.customerapi.infra.mysql.ConnectionManager;
import com.bugra.app.customerapi.domain.category.Category;
import com.bugra.app.customerapi.domain.category.ICategoryRepository;
import com.bugra.app.customerapi.domain.product.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CategoryRepositoryImpl implements ICategoryRepository {


    @Override
    public ArrayList<Category> getAllCategoryFromDb() throws SQLException {

        int CategoryID;
        String CategoryName;
        String Description;

        ArrayList<Category> categories = new ArrayList<>();
        String sql = "select * from trendyoldb.categories";

        Connection connection = ConnectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery(); // table

        while (resultSet.next()) {
            CategoryID = resultSet.getInt("CategoryID");
            CategoryName = resultSet.getString("CategoryName");
            Description = resultSet.getString("Description");
            Category category = new Category(CategoryID, CategoryName, Description);
            categories.add(category);
        }

        return categories;
    }

    @Override
    public Category getCategoryByID(Integer id) throws SQLException {

        int CategoryID;
        String CategoryName;
        String Description;

        String sql = String.format("select * from trendyoldb.categories where CategoryID = %d", id);

        Connection connection = ConnectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next(); // headere geç
        CategoryID = resultSet.getInt("CategoryID");
        CategoryName = resultSet.getString("CategoryName");
        Description = resultSet.getString("Description");

        // id'si (1) olan categorymiz.
        Category category = new Category(CategoryID, CategoryName, Description);

        // id'si 1 olan categorinin ürünlerini bulmak
        String sqlCategoryProducts = String.format("select * from products where category_id = %d", category.getCategoryID());
        PreparedStatement sqlCategoryProductsStatement = connection.prepareStatement(sqlCategoryProducts);
        ResultSet categoryProductsResultSet = sqlCategoryProductsStatement.executeQuery();

        List<Product> products = new ArrayList<>();

        while (categoryProductsResultSet.next()) {
            int id1 = categoryProductsResultSet.getInt("id"); // primary
            String name1 = categoryProductsResultSet.getString("name"); // primary
            int supplierId = categoryProductsResultSet.getInt("supplier_id"); // primary
            int categoryId1 = categoryProductsResultSet.getInt("category_id"); // primary
            String unit1 = categoryProductsResultSet.getString("unit"); // primary
            double price1 = categoryProductsResultSet.getDouble("price"); // parse string

            Product product = new Product(id1, name1, supplierId, categoryId1, unit1, price1);
            product.setCategory(category); //key

            products.add(product); // tüm fermente 1 olan 2 ürün, 3 ür
        }

        category.setProductList(products); // key

        return category;
    }

    @Override
    public ArrayList<Category> getCategoryByCategoryName(String name) throws SQLException {

        int CategoryID;
        String CategoryName;
        String Description;

        String sql = String.format("select * from trendyoldb.categories where CategoryName = '%s'", name);
        ArrayList<Category> categories = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            CategoryID = resultSet.getInt("CategoryID");
            CategoryName = resultSet.getString("CategoryName");
            Description = resultSet.getString("Description");
            Category category = new Category(CategoryID, CategoryName, Description);
            categories.add(category);
        }
        return categories;

    }


    @Override
    public Category getCategoryByIdWithProductWithInnerJoinQuery(Integer id) throws SQLException {
        String sqlWord = String.format("select * from products inner join categories on products.category_id = categories.CategoryID where category_id = %d", id);
        Connection connection = ConnectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(sqlWord);
        ResultSet resultSet = preparedStatement.executeQuery();

        List<Product> products = new ArrayList<>();
        List<Category> categories = new ArrayList<>();

        while (resultSet.next()) {
            int id1 = resultSet.getInt("id");
            String productName = resultSet.getString("products.name");
            String categoryName = resultSet.getString("CategoryName");

            // values to model
            products.add(new Product(id1, productName, 0, resultSet.getInt("CategoryId"), null, 0.0));
            categories.add(new Category(resultSet.getInt("CategoryId"), categoryName, resultSet.getString("Description")));
        }

        // key
        Category category = categories.get(0);
        category.setProductList(products);
        return category;
    }

    @Override
    public void deleteCategoryWithDeleteRequest(DeleteCategoryRequest deleteCategoryRequest) throws SQLException {
        String sqlWord = String.format("delete from categories where CategoryID = %d", deleteCategoryRequest.getId());
        ConnectionManager.getConnection().prepareStatement(sqlWord).execute();
    }


    @Override
    public void updateCategoryWithUpdateRequest(UpdateCategoryRequest updateCategoryRequest) throws SQLException {
        String sqlWord = String.format("update trendyoldb.categories set CategoryName = '%s', Description = '%s' where CategoryID = %d", updateCategoryRequest.getCategoryName(), updateCategoryRequest.getDescription(), updateCategoryRequest.getId());
        ConnectionManager.getConnection().prepareStatement(sqlWord).execute();
    }

    @Override
    public void createCategoryWithCreateRequest(CreateCategoryRequest createCategoryRequest) throws SQLException {
        String sqlWord = String.format("insert into trendyoldb.categories (CategoryName, Description) values ('%s','%s')", createCategoryRequest.getCategoryName(), createCategoryRequest.getDescription());

        Connection connection = ConnectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(sqlWord);
        preparedStatement.execute();

    }


}
