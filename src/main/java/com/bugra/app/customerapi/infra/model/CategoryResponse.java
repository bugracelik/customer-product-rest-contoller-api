package com.bugra.app.customerapi.infra.model;

import java.util.ArrayList;
import java.util.List;

public class CategoryResponse { // dto -> data transfer object.
    private String name;
    private Integer categoryId;
    private String description;
    private List<ProductResponse> products = new ArrayList<>(); // key nokta // vermiş oldu

    public CategoryResponse() {
    }

    public CategoryResponse(String name, Integer categoryId, String description, List<ProductResponse> products) {
        this.name = name;
        this.categoryId = categoryId;
        this.description = description;
        this.products = products;
    }

    public void setName(String categoryName) {
        this.name = categoryName;
    }

    // get
    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ProductResponse> getProducts() {
        return products;
    }

    public void setProducts(List<ProductResponse> products) {
        this.products = products;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "CategoryResponse{" +
                "name='" + name + '\'' +
                ", categoryId=" + categoryId +
                ", description='" + description + '\'' +
                ", products=" + products +
                '}';
    }
}
