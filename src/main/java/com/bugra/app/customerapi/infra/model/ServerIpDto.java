package com.bugra.app.customerapi.infra.model;

public class ServerIpDto {

    private static final String serverIp = "207.154.216.113";

    public String getServerIp() {
        return serverIp;
    }

    @Override
    public String toString() {
        return "ServerIpDto{" +
                "serverIp='" + serverIp + '\'' +
                '}';
    }
}
