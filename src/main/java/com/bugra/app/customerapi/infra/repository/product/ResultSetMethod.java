package com.bugra.app.customerapi.infra.repository.product;

import com.bugra.app.customerapi.domain.category.Category;
import com.bugra.app.customerapi.domain.product.Product;
import com.bugra.app.customerapi.infra.repository.category.CategoryRepositoryImpl;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ResultSetMethod {

    public static List<Product> getAllProductWithResultSet(ResultSet resultSet) throws SQLException {
        // key-point
        CategoryRepositoryImpl categoryRepository = new CategoryRepositoryImpl();
        List<Product> result = new ArrayList<>();
        int categoryId = 0;
        Product product = null;

        while (resultSet.next()) {
            // satır
            int id = resultSet.getInt("id"); // primary
            String name = resultSet.getString("name"); // primary
            int supplierId = resultSet.getInt("supplier_id"); // primary
            categoryId = resultSet.getInt("category_id"); // primary
            String unit = resultSet.getString("unit"); // primary
            double price = resultSet.getDouble("price"); // parse string
            product = new Product(id, name, supplierId, categoryId, unit, price);
            Category category = categoryRepository.getCategoryByID(categoryId);
            product.setCategory(category);
            result.add(product);
        }

        return result;
    }

    public static Product getAllProductWithResultSetUniq(ResultSet resultSet) throws SQLException {
        // key-point
        CategoryRepositoryImpl categoryRepository = new CategoryRepositoryImpl();
        Product product = null;
        resultSet.next();

        int id = resultSet.getInt("id"); // primary
        String name = resultSet.getString("name"); // primary
        int supplierId = resultSet.getInt("supplier_id"); // primary
        int categoryId = resultSet.getInt("category_id"); // primary
        String unit = resultSet.getString("unit"); // primary
        double price = resultSet.getDouble("price"); // parse string
        product = new Product(id, name, supplierId, categoryId, unit, price);
        Category category = categoryRepository.getCategoryByID(categoryId);
        product.setCategory(category);

        return product;

    }

}
