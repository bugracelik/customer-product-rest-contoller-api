package com.bugra.app.customerapi.infra.repository.supplier;

import com.bugra.app.customerapi.infra.mysql.ConnectionManager;
import com.bugra.app.customerapi.domain.supplier.ISupllierRepository;
import com.bugra.app.customerapi.domain.supplier.Supplier;
import com.bugra.app.customerapi.infra.request.supplier.CreateSupplierRequest;
import com.bugra.app.customerapi.infra.request.supplier.UpdateSupplierRequest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SupplierRepositoryImpl implements ISupllierRepository {

    @Override
    public ArrayList<Supplier> getAllSuppliersFromDb() throws SQLException {

        int SupplierId;
        String SupplierName;
        String ContactName;
        String Address;
        String City;
        String PostalCode;
        String Country;

        String sql = "select * from suppliers";
        ArrayList<Supplier> arrayList = new ArrayList<>();

        Connection connection = ConnectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            SupplierId = resultSet.getInt("SupplierId");
            SupplierName = resultSet.getString("SupplierName");
            ContactName = resultSet.getString("ContactName");
            Address = resultSet.getString("Address");
            City = resultSet.getString("City");
            PostalCode = resultSet.getString("PostalCode");
            Country = resultSet.getString("Country");
            Supplier supplier = new Supplier(SupplierId, SupplierName, ContactName, Address, City, PostalCode, Country);
            arrayList.add(supplier);
        }

        return arrayList;
    }

    @Override
    public Supplier getSuppliersByID(Integer id) throws SQLException {

        int SupplierId;
        String SupplierName;
        String ContactName;
        String Address;
        String City;
        String PostalCode;
        String Country;

        String sql = String.format("select * from suppliers where SupplierID = %d", id);

        Connection connection = ConnectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();

        resultSet.next();

        SupplierId = resultSet.getInt("SupplierId");
        SupplierName = resultSet.getString("SupplierName");
        ContactName = resultSet.getString("ContactName");
        Address = resultSet.getString("Address");
        City = resultSet.getString("City");
        PostalCode = resultSet.getString("PostalCode");
        Country = resultSet.getString("Country");
        return new Supplier(SupplierId, SupplierName, ContactName, Address, City, PostalCode, Country);
    }
    @Override
    public Supplier getSuppliersByContactName(String name) throws SQLException {

        int SupplierId;
        String SupplierName;
        String ContactName;
        String Address;
        String City;
        String PostalCode;
        String Country;

        String sql = String.format("select * from suppliers where ContactName = '%s'", name);

        Connection connection = ConnectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();

        resultSet.next();

        SupplierId = resultSet.getInt("SupplierId");
        SupplierName = resultSet.getString("SupplierName");
        ContactName = resultSet.getString("ContactName");
        Address = resultSet.getString("Address");
        City = resultSet.getString("City");
        PostalCode = resultSet.getString("PostalCode");
        Country = resultSet.getString("Country");
        return new Supplier(SupplierId, SupplierName, ContactName, Address, City, PostalCode, Country);
    }
    @Override
    public void createSupplier(String supplierName, String contactName, String adress, String city, String postalCode, String country) throws SQLException {
        String sql = String.format("insert into suppliers (SupplierName, ContactName, Address, City, PostalCode, Country) values ('%s', '%s', '%s', '%s', '%s', '%s')", supplierName, contactName, adress, city, postalCode, country);
        ConnectionManager.getConnection().prepareStatement(sql).execute();
    }
    @Override
    public void updateSupplier(Integer supplierId, String supplierName, String contactName, String adress, String city, String postalCode, String country) throws SQLException {
        String sql = String.format("update suppliers set SupplierName = '%s', ContactName = '%s', Address = '%s', City = '%s', PostalCode = '%s', Country = '%s' where SupplierID = %d", supplierName, contactName, adress, city, postalCode, country, supplierId);
        ConnectionManager.getConnection().prepareStatement(sql).execute();
    }
    @Override
    public void deleteSupplierById(int supplierId) throws SQLException {
        String sql = String.format("delete from suppliers where SupplierID = %d", supplierId);
        ConnectionManager.getConnection().prepareStatement(sql).execute();
    }

    @Override
    public void createSupplier(CreateSupplierRequest createSupplierRequest) throws SQLException {
        String sqlWord = String.format("insert into suppliers (SupplierName, ContactName, Address, City, PostalCode, Country) values ('%s', '%s', '%s', '%s', '%s', '%s')",
                createSupplierRequest.getSupplierName(),
                createSupplierRequest.getContactName(),
                createSupplierRequest.getAddress(),
                createSupplierRequest.getCity(),
                createSupplierRequest.getPostalCode(),
                createSupplierRequest.getCountry());

        ConnectionManager.getConnection().prepareStatement(sqlWord).execute();
    }

    @Override
    public void updateSupplier(UpdateSupplierRequest updateSupplierRequest, int id) throws SQLException {
        String sqlWord = String.format("update suppliers set SupplierName = '%s', ContactName = '%s', Address = '%s', City = '%s', PostalCode = '%s', Country = '%s' where SupplierID = %d",
                updateSupplierRequest.getSupplierName(),
                updateSupplierRequest.getContactName(),
                updateSupplierRequest.getAddress(),
                updateSupplierRequest.getCity(),
                updateSupplierRequest.getPostalCode(),
                updateSupplierRequest.getCountry(),
                id);
        ConnectionManager.getConnection().prepareStatement(sqlWord).execute();
    }
}
