package com.bugra.app.customerapi.infra.controller.healthy;


import com.bugra.app.customerapi.infra.mysql.ConnectionManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;

@RestController
public class HealthyController {

    @GetMapping("ok")
    public String serverInformation(){
        return "Server is working";
    }

    @GetMapping("mysql")
    public String mysqlInformation(){
        try {
            Connection connection = ConnectionManager.getConnection();
            return "Mysql server is working";
        } catch (Exception e) {
            e.printStackTrace();
            return "Mysql server isn't working";
        }
    }

}
