package com.bugra.app.customerapi.infra.request.product;

public class UpdateProductRequest {

    private String name;
    private int supplier_id;
    private int category_id;
    private String unit;
    private double price;

    public UpdateProductRequest() {
    }

    public UpdateProductRequest(String name, int supplier_id, int category_id, String unit, double price) {
        this.name = name;
        this.supplier_id = supplier_id;
        this.category_id = category_id;
        this.unit = unit;
        this.price = price;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSupplier_id() {
        return supplier_id;
    }

    public void setSupplier_id(int supplier_id) {
        this.supplier_id = supplier_id;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
