//categori

INSERT INTO trendyoldb.categories (CategoryID, CategoryName, Description) VALUES (1, 'fermente', 'fermente icki');
INSERT INTO trendyoldb.categories (CategoryID, CategoryName, Description) VALUES (2, 'Condiments-foo', 'Sweet and savory sauces, relishes, spreads, and seasonings');
INSERT INTO trendyoldb.categories (CategoryID, CategoryName, Description) VALUES (3, 'Confections', 'Desserts, candies, and sweet breads');
INSERT INTO trendyoldb.categories (CategoryID, CategoryName, Description) VALUES (4, 'Dairy Product	', 'Cheeses');
INSERT INTO trendyoldb.categories (CategoryID, CategoryName, Description) VALUES (5, 'Grains/Cereals', 'Breads, crackers, pasta, and cereal');
INSERT INTO trendyoldb.categories (CategoryID, CategoryName, Description) VALUES (6, 'Meat/Poultry	', 'Prepared meats');
INSERT INTO trendyoldb.categories (CategoryID, CategoryName, Description) VALUES (7, 'eglence', 'top, bilardo');
INSERT INTO trendyoldb.categories (CategoryID, CategoryName, Description) VALUES (8, 'Seafood', 'Seaweed and fish');
INSERT INTO trendyoldb.categories (CategoryID, CategoryName, Description) VALUES (9, 'beyaz eşya', 'çamaşır makinesi, bulaşık makinesi, buz dolabı');
INSERT INTO trendyoldb.categories (CategoryID, CategoryName, Description) VALUES (11, 'bilgisayar', 'laptop,dizüstü,monitor');
INSERT INTO trendyoldb.categories (CategoryID, CategoryName, Description) VALUES (16, 'ahsap', 'mobilya, mobilya, tornavida');
INSERT INTO trendyoldb.categories (CategoryID, CategoryName, Description) VALUES (19, 'asda', 'sdasd');
INSERT INTO trendyoldb.categories (CategoryID, CategoryName, Description) VALUES (23, 'altın', 'mücevher');
INSERT INTO trendyoldb.categories (CategoryID, CategoryName, Description) VALUES (24, 'gümüş', 'mücevher');

//product

INSERT INTO trendyoldb.products (id, name, supplier_id, category_id, unit, price) VALUES (2, 'kombucha', 4, 1, '4', 80);
INSERT INTO trendyoldb.products (id, name, supplier_id, category_id, unit, price) VALUES (4, 'ahşap', 5, 16, '16', 850);
INSERT INTO trendyoldb.products (id, name, supplier_id, category_id, unit, price) VALUES (5, 'kereste', 5, 16, '16', 850);
INSERT INTO trendyoldb.products (id, name, supplier_id, category_id, unit, price) VALUES (6, 'Schmidt', 2, 4, '89', 22.2);
INSERT INTO trendyoldb.products (id, name, supplier_id, category_id, unit, price) VALUES (7, 'bugra', 5, 16, '123', 12);
INSERT INTO trendyoldb.products (id, name, supplier_id, category_id, unit, price) VALUES (8, 'yeni', 5, 16, '123', 12);
INSERT INTO trendyoldb.products (id, name, supplier_id, category_id, unit, price) VALUES (9, 'kombucha-foo', 4, 1, '2', 40);


//suppliers

INSERT INTO trendyoldb.suppliers (SupplierID, SupplierName, ContactName, Address, City, PostalCode, Country) VALUES (1, 'Exotic Liquid', 'Charlotte Cooper', '49 Gilbert St.', 'Londona', 'EC1 4SD', 'UK');
INSERT INTO trendyoldb.suppliers (SupplierID, SupplierName, ContactName, Address, City, PostalCode, Country) VALUES (2, 'New Orleans Cajun Delights', 'Shelley Burke', 'P.O. Box 78934', 'New Orleans', '70117', 'USA');
INSERT INTO trendyoldb.suppliers (SupplierID, SupplierName, ContactName, Address, City, PostalCode, Country) VALUES (3, 'Grandma Kelly''s Homestead	', 'Regina Murphy', '707 Oxford Rd.', 'Ann Arbor', '48104', 'ENG');
INSERT INTO trendyoldb.suppliers (SupplierID, SupplierName, ContactName, Address, City, PostalCode, Country) VALUES (4, 'bugra', 'delibugra', 'tarabta', 'istanbul', '34457', 'Türkiye');
INSERT INTO trendyoldb.suppliers (SupplierID, SupplierName, ContactName, Address, City, PostalCode, Country) VALUES (5, 'tugberk', 'mühendis', 'maden', 'izmir', '32141', 'abd');
INSERT INTO trendyoldb.suppliers (SupplierID, SupplierName, ContactName, Address, City, PostalCode, Country) VALUES (6, 'Alfred Schmidt', 'Frankfurt', 'ASDASD', 'fafda', '13', 'dasd');


//customers

INSERT INTO trendyoldb.customers (CustomerID, CustomerName, CustomerSurname, CustomerAdress, CustomerCity, CustomerEmail, CustomerPassword) VALUES (2, 'Tugberk', 'çelik', 'maden mahallesi', 'çanakkale', 'mrtugberkcelik@gmail.com', '19291');
INSERT INTO trendyoldb.customers (CustomerID, CustomerName, CustomerSurname, CustomerAdress, CustomerCity, CustomerEmail, CustomerPassword) VALUES (3, 'Alper', 'eren', 'vilayet mahallesi', 'ankara', 'alper@gmail.com', 'loratcilpe');
INSERT INTO trendyoldb.customers (CustomerID, CustomerName, CustomerSurname, CustomerAdress, CustomerCity, CustomerEmail, CustomerPassword) VALUES (4, 'ismail', 'bülbül', 'kazım sokak mahallesi', 'ısparta', 'yakısıklıboy@gmail.com', 'uzunpassqpr');
INSERT INTO trendyoldb.customers (CustomerID, CustomerName, CustomerSurname, CustomerAdress, CustomerCity, CustomerEmail, CustomerPassword) VALUES (5, 'ayşe', 'kızılsultan', 'papatya sokak', 'venedik', 'aysegul@gmail.com', '123loratcilpre');
INSERT INTO trendyoldb.customers (CustomerID, CustomerName, CustomerSurname, CustomerAdress, CustomerCity, CustomerEmail, CustomerPassword) VALUES (8, 'sümeyye', 'keskin', 'kagıthane mahallesi', 'düzce', 'sümeyyekeskin@gmail.com', '19681983');
INSERT INTO trendyoldb.customers (CustomerID, CustomerName, CustomerSurname, CustomerAdress, CustomerCity, CustomerEmail, CustomerPassword) VALUES (9, 'hasan kerem', 'uzunlar', 'bagcılar mahallesi', 'izmir', 'hasankerem@gmail.com', 'hasankerem123');
INSERT INTO trendyoldb.customers (CustomerID, CustomerName, CustomerSurname, CustomerAdress, CustomerCity, CustomerEmail, CustomerPassword) VALUES (10, 'ertan', 'çelik', 'tarabya mahallesi', 'istanbul', 'ertan@gmail.com', 'ertan23123');
INSERT INTO trendyoldb.customers (CustomerID, CustomerName, CustomerSurname, CustomerAdress, CustomerCity, CustomerEmail, CustomerPassword) VALUES (15, 'berna', 'tepe', 'ankara mahallesi', 'izmir', 'uguzberna@gmail.com', '1231aqwe');
INSERT INTO trendyoldb.customers (CustomerID, CustomerName, CustomerSurname, CustomerAdress, CustomerCity, CustomerEmail, CustomerPassword) VALUES (18, 'XSpring', 'çelik', 'maden mahallesi', 'çanakkale', 'mrtugberkceliks@gmail.com', '19291');
INSERT INTO trendyoldb.customers (CustomerID, CustomerName, CustomerSurname, CustomerAdress, CustomerCity, CustomerEmail, CustomerPassword) VALUES (19, 'XSpring', 'çelik', 'maden mahallesi', 'çanakkale', 'x@gmail.com', '19291');
INSERT INTO trendyoldb.customers (CustomerID, CustomerName, CustomerSurname, CustomerAdress, CustomerCity, CustomerEmail, CustomerPassword) VALUES (20, 'XSpring', 'çelik', 'maden mahallesi', 'çanakkale', 'y@gmail.com', '19291');