
/* kategori tablosu
create table categories
(
    CategoryID   int auto_increment
        primary key,
    CategoryName varchar(100) null,
    Description  varchar(150) null,
    constraint categories_CategoryName_uindex
        unique (CategoryName)
);

create index index_category_id
    on categories (CategoryID);
*/

/* customer tablosu

create table customers
(
    CustomerID       int auto_increment
        primary key,
    CustomerName     varchar(50)  null,
    CustomerSurname  varchar(50)  null,
    CustomerAdress   varchar(100) null,
    CustomerCity     varchar(50)  null,
    CustomerEmail    varchar(50)  null,
    CustomerPassword varchar(50)  null,
    constraint customers_CustomerEmail_uindex
        unique (CustomerEmail)
);

*/

/*
producst tablosu

create table products
(
    id          int auto_increment
        primary key,
    name        varchar(50) not null,
    supplier_id int         not null,
    category_id int         not null,
    unit        varchar(50) null,
    price       double      null,
    constraint products_categories_CategoryID_fk
        foreign key (category_id) references categories (CategoryID),
    constraint products_suppliers_SupplierID_fk
        foreign key (supplier_id) references suppliers (SupplierID)
);
*/

/*
supplier tablosu
-- auto-generated definition
create table suppliers
(
    SupplierID   int auto_increment
        primary key,
    SupplierName varchar(100) null,
    ContactName  varchar(100) null,
    Address      varchar(100) null,
    City         varchar(100) null,
    PostalCode   varchar(100) null,
    Country      varchar(100) null
);

*/
